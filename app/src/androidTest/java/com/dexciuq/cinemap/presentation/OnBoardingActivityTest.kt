package com.dexciuq.cinemap.presentation

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.viewpager2.widget.ViewPager2
import com.dexciuq.cinemap.R

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

@RunWith(AndroidJUnit4::class)
class OnBoardingActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(OnBoardingActivity::class.java)

    private lateinit var scenario: ActivityScenario<OnBoardingActivity>

    @Before
    fun setUp() {
        scenario = activityRule.scenario
    }

    @Test
    fun clickSkipButton_shouldCompleteOnBoarding() {
        onView(withId(R.id.skip_button)).perform(click())
        onView(withId(R.id.main_activity_layout))
            .check(matches(isDisplayed()))
    }

    @Test
    fun clickSkipButtonNoInternetCase_shouldCompleteOnBoarding() {
        onView(withId(R.id.skip_button)).perform(click())
        onView(withId(R.id.no_internet_activity_layout))
            .check(matches(isDisplayed()))
    }


    @Test
    fun clickNextButtonTwice_shouldLeftOne() {
        val times = 2
        repeat(times) {
            onView(withId(R.id.next_button)).perform(click())
        }
        onView(withId(R.id.on_boarding_view_pager)).check { view, _ ->
            val viewPager = view as ViewPager2
            val lastPagePosition = viewPager.adapter?.itemCount?.minus(1) ?: 0
            assert(viewPager.currentItem == lastPagePosition)
        }
    }
}