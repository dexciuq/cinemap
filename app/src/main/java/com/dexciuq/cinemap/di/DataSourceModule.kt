package com.dexciuq.cinemap.di

import com.dexciuq.cinemap.data.datasource.local.LocalDataSource
import com.dexciuq.cinemap.data.datasource.remote.KinoPoiskDataSource
import com.dexciuq.cinemap.data.datasource.MovieDataSource
import com.dexciuq.cinemap.data.mapper.GenreMapper
import com.dexciuq.cinemap.data.mapper.Mapper
import com.dexciuq.cinemap.data.mapper.MovieEntityMapper
import com.dexciuq.cinemap.data.mapper.MovieMapper
import com.dexciuq.cinemap.data.model.local.MovieEntity
import com.dexciuq.cinemap.data.model.remote.GenresDtoItem
import com.dexciuq.cinemap.data.model.remote.MovieDto
import com.dexciuq.cinemap.domain.model.Movie
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface DataSourceModule {

    @Binds
    fun provideRemoteMovieDataSource(kinoPoiskDataSource: KinoPoiskDataSource): MovieDataSource.Remote

    @Binds
    fun provideLocalMovieDataSource(localDataSource: LocalDataSource): MovieDataSource.Local
}