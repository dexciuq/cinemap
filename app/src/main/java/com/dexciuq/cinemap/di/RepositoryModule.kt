package com.dexciuq.cinemap.di

import com.dexciuq.cinemap.data.repository.MovieRepositoryImpl
import com.dexciuq.cinemap.domain.repository.MovieRepository
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {

    @Binds
    fun provideMovieRepository(movieRepositoryImpl: MovieRepositoryImpl): MovieRepository
}