package com.dexciuq.cinemap.di

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.lifecycle.viewmodel.InitializerViewModelFactoryBuilder
import com.bumptech.glide.Glide
import com.dexciuq.cinemap.domain.usecase.AddFavoriteUseCase
import com.dexciuq.cinemap.domain.usecase.DeleteFavoriteUseCase
import com.dexciuq.cinemap.domain.usecase.GetFavoriteUseCase
import com.dexciuq.cinemap.domain.usecase.GetMovieUseCase
import com.dexciuq.cinemap.domain.usecase.GetMoviesByGenreUseCase
import com.dexciuq.cinemap.domain.usecase.GetFavoritesUseCase
import com.dexciuq.cinemap.domain.usecase.GetGenresUseCase
import com.dexciuq.cinemap.domain.usecase.GetMoviesBySearchUseCase
import com.dexciuq.cinemap.presentation.loader.GlideImageLoader
import com.dexciuq.cinemap.presentation.loader.ImageLoader
import com.dexciuq.cinemap.presentation.screen.category.CategoryViewModel
import com.dexciuq.cinemap.presentation.screen.dashboard.DashboardViewModel
import com.dexciuq.cinemap.presentation.screen.detail.DetailViewModel
import com.dexciuq.cinemap.presentation.screen.favorite.FavoriteViewModel
import com.dexciuq.cinemap.presentation.screen.result.ResultViewModel
import com.dexciuq.cinemap.presentation.screen.seeall.SeeAllViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
interface PresentationModule {

    @Binds
    fun provideImageLoader(glideImageLoader: GlideImageLoader): ImageLoader

    companion object {
        @Provides
        fun provideGlide(context: Context): Glide {
            return Glide.get(context)
        }


        @Provides
        @Named("favorite")
        fun provideFavoriteViewModelFactory(
            getFavoritesUseCase: GetFavoritesUseCase
        ): ViewModelProvider.Factory {
            return viewModelBuilder {
                FavoriteViewModel(getFavoritesUseCase)
            }
        }

        @Provides
        @Named("dashboard")
        fun provideDashboardViewModelFactory(
            getMoviesByGenreUseCase: GetMoviesByGenreUseCase,
        ): ViewModelProvider.Factory {
            return viewModelBuilder {
                DashboardViewModel(getMoviesByGenreUseCase)
            }
        }

        @Provides
        @Named("seeall")
        fun provideSeeAllViewModelFactory(
            getMoviesByGenreUseCase: GetMoviesByGenreUseCase,
        ): ViewModelProvider.Factory {
            return viewModelBuilder {
                SeeAllViewModel(getMoviesByGenreUseCase)
            }
        }

        @Provides
        @Named("detail")
        fun provideDetailViewModelFactory(
            addFavoriteUseCase: AddFavoriteUseCase,
            deleteFavoriteUseCase: DeleteFavoriteUseCase,
            getFavoriteUseCase: GetFavoriteUseCase,
            getMovieUseCase: GetMovieUseCase
        ): ViewModelProvider.Factory {
            return viewModelBuilder {
                DetailViewModel(
                    getFavoriteUseCase,
                    addFavoriteUseCase,
                    deleteFavoriteUseCase,
                    getMovieUseCase
                )
            }
        }

        @Provides
        @Named("category")
        fun provideCategoryViewModelFactory(
            getGenresUseCase: GetGenresUseCase
        ): ViewModelProvider.Factory {
            return viewModelBuilder {
                CategoryViewModel(getGenresUseCase)
            }
        }

        @Provides
        @Named("result")
        fun provideResultViewModelFactory(
            getMoviesBySearchUseCase: GetMoviesBySearchUseCase
        ): ViewModelProvider.Factory {
            return viewModelBuilder {
                ResultViewModel(getMoviesBySearchUseCase)
            }
        }

        private inline fun <reified VM : ViewModel> viewModelBuilder(
            noinline initializer: CreationExtras.() -> VM
        ): ViewModelProvider.Factory {
            return InitializerViewModelFactoryBuilder().apply {
                addInitializer(VM::class, initializer)
            }.build()
        }
    }
}