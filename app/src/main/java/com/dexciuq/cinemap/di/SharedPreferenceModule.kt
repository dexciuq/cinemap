package com.dexciuq.cinemap.di

import android.content.Context
import android.content.SharedPreferences
import com.dexciuq.cinemap.util.Const
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SharedPreferenceModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(
            Const.SHARED_PREF_NAME,
            Context.MODE_PRIVATE
        )
    }
}