package com.dexciuq.cinemap.di

import android.content.Context
import androidx.room.Room
import com.dexciuq.cinemap.util.Const
import com.dexciuq.cinemap.data.datasource.local.AppDatabase
import com.dexciuq.cinemap.data.datasource.local.MovieDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppDatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context = context.applicationContext,
            klass = AppDatabase::class.java,
            name = Const.DATABASE_NAME,
        ).build()
    }

    @Provides
    fun provideNotificationDao(database: AppDatabase): MovieDao {
        return database.movieDao()
    }
}