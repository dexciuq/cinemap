package com.dexciuq.cinemap.di

import com.dexciuq.cinemap.util.Const
import com.dexciuq.cinemap.data.datasource.remote.KinoPoiskApiService
import com.dexciuq.cinemap.data.datasource.remote.XApiKeyInterceptor
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    @Named("apiKey")
    fun provideApiKey(): String = Const.API_KEY

    @Provides
    @Singleton
    @Named("baseUrl")
    fun provideBaseUrl(): String = Const.BASE_URL

    @Provides
    @Singleton
    fun provideGsonConverterFactory(): GsonConverterFactory =
        GsonConverterFactory.create()

    @Provides
    @Singleton
    @IntoSet
    fun provideXApiKeyInterceptor(@Named("apiKey") apiKey: String): Interceptor {
        return XApiKeyInterceptor(apiKey)
    }

    @Singleton
    @Provides
    fun provideHttpClient(interceptors: Set<@JvmSuppressWildcards Interceptor>): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .apply {
                interceptors().addAll(interceptors)
            }
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory,
        @Named("baseUrl") baseUrl: String
    ): Retrofit = Retrofit.Builder()
            .addConverterFactory(gsonConverterFactory)
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .build()

    @Provides
    @Singleton
    fun provideKinoPoiskApiService(retrofit: Retrofit): KinoPoiskApiService =
        retrofit.create(KinoPoiskApiService::class.java)
}