package com.dexciuq.cinemap.di

import android.content.Context
import com.dexciuq.cinemap.presentation.MainActivity
import com.dexciuq.cinemap.presentation.NoInternetActivity
import com.dexciuq.cinemap.presentation.OnBoardingActivity
import com.dexciuq.cinemap.presentation.screen.category.CategoryFragment
import com.dexciuq.cinemap.presentation.screen.dashboard.DashboardFragment
import com.dexciuq.cinemap.presentation.screen.detail.DetailFragment
import com.dexciuq.cinemap.presentation.screen.favorite.FavoriteFragment
import com.dexciuq.cinemap.presentation.screen.result.ResultFragment
import com.dexciuq.cinemap.presentation.screen.seeall.SeeAllFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppDatabaseModule::class,
    DataSourceModule::class,
    NetworkModule::class,
    RepositoryModule::class,
    SharedPreferenceModule::class,
    PresentationModule::class,
])
interface AppComponent {

    fun inject(activity: MainActivity)
    fun inject(activity: OnBoardingActivity)
    fun inject(activity: NoInternetActivity)
    fun inject(fragment: DashboardFragment)
    fun inject(fragment: FavoriteFragment)
    fun inject(fragment: SeeAllFragment)
    fun inject(fragment: DetailFragment)
    fun inject(fragment: CategoryFragment)
    fun inject(fragment: ResultFragment)

    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun bindContext(context: Context): Builder
    }
}