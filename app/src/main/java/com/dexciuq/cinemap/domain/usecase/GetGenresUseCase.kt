package com.dexciuq.cinemap.domain.usecase

import com.dexciuq.cinemap.domain.repository.MovieRepository
import javax.inject.Inject

class GetGenresUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) {
    suspend operator fun invoke() = movieRepository.getGenres()
}