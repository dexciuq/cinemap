package com.dexciuq.cinemap.domain.usecase

import com.dexciuq.cinemap.domain.repository.MovieRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetMoviesByGenreUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) {
    suspend operator fun invoke(genre: String, limit: Int) = withContext(Dispatchers.IO) {
        movieRepository.getMoviesByGenre(genre, limit)
    }
}