package com.dexciuq.cinemap.domain.usecase

import com.dexciuq.cinemap.domain.repository.MovieRepository
import javax.inject.Inject

class GetFavoritesUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) {
    operator fun invoke() = movieRepository.getFavorites()
}