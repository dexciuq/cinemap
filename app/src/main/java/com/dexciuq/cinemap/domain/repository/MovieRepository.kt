package com.dexciuq.cinemap.domain.repository

import androidx.lifecycle.LiveData
import com.dexciuq.cinemap.data.model.local.MovieEntity
import com.dexciuq.cinemap.domain.model.Movie

interface MovieRepository {
    suspend fun getMovie(id: Long): Movie
    suspend fun getMoviesBySearch(query: String): List<Movie>
    suspend fun getMoviesByGenre(genre: String, limit: Int): List<Movie>
    suspend fun getGenres(): List<String>
    fun getFavorites(): LiveData<List<MovieEntity>>
    suspend fun getFavorite(id: Long): MovieEntity?
    suspend fun addToFavorites(movie: Movie)
    suspend fun deleteFromFavorites(id: Long)
}