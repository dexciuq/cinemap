package com.dexciuq.cinemap.domain.usecase

import com.dexciuq.cinemap.domain.repository.MovieRepository
import javax.inject.Inject

class DeleteFavoriteUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) {
    suspend operator fun invoke(id: Long) = movieRepository.deleteFromFavorites(id)
}