package com.dexciuq.cinemap.domain.usecase

import com.dexciuq.cinemap.domain.model.Movie
import com.dexciuq.cinemap.domain.repository.MovieRepository
import javax.inject.Inject

class AddFavoriteUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) {
    suspend operator fun invoke(movie: Movie) = movieRepository.addToFavorites(movie)
}