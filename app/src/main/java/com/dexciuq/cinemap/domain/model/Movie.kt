package com.dexciuq.cinemap.domain.model

import com.dexciuq.cinemap.data.model.remote.Person
import com.dexciuq.cinemap.data.model.remote.Rating
import com.dexciuq.cinemap.data.model.remote.Votes

data class Movie(
    val id: Long,
    val name: String,
    val alternativeName: String,
    val description: String,
    val shortDescription: String,
    val slogan: String,
    val year: Int,
    val countries: List<String>,
    val duration: Int,
    val poster: String,
    val backdrop: String,
    val logo: String,
    val genres: List<String>,
    val cast: List<Person>,
    val budget: String,
    val fees: String,
    val rating: Rating,
    val votes: Votes,
)
