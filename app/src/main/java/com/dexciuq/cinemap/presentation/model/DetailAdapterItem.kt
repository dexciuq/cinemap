package com.dexciuq.cinemap.presentation.model

import com.dexciuq.cinemap.data.model.remote.Person

sealed interface DetailAdapterItem {

    data class Backdrop(
        val movieId: Long,
        val imageURL: String,
        val name: String,
        val info: String,
        val description: String,
    ) : DetailAdapterItem

    data class Option(
        val key: String,
        val value: String,
    ) : DetailAdapterItem

    data class Cast(
        val actors: List<Person>
    ) : DetailAdapterItem
}