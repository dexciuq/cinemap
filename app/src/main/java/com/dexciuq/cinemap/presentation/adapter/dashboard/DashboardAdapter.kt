package com.dexciuq.cinemap.presentation.adapter.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.cinemap.R
import com.dexciuq.cinemap.databinding.ItemBannerBinding
import com.dexciuq.cinemap.databinding.ItemHeaderBinding
import com.dexciuq.cinemap.databinding.ItemMovieListBinding
import com.dexciuq.cinemap.presentation.loader.ImageLoader
import com.dexciuq.cinemap.presentation.model.DashboardAdapterItem

class DashboardAdapter(
    private val imageLoader: ImageLoader,
    private val onHeaderClick: (String) -> Unit,
    private val onListItemClick: (Long) -> Unit,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    // TODO delegate adapter

    private var items: List<DashboardAdapterItem> = emptyList()

    fun submitList(newItems: List<DashboardAdapterItem>) {
        val diffResult = DiffUtil.calculateDiff(DashboardDiffCallback(items, newItems))
        items = newItems
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            R.layout.item_banner -> BannerViewHolder(
                ItemBannerBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
            )
            R.layout.item_header -> HeaderViewHolder(
                ItemHeaderBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
            )
            R.layout.item_movie_list -> MovieListViewHolder(
                ItemMovieListBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
            )
            else -> error("unknown viewType")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is BannerViewHolder -> holder.bind(items[position] as DashboardAdapterItem.Banner)
            is HeaderViewHolder -> holder.bind(items[position] as DashboardAdapterItem.Header)
            is MovieListViewHolder -> holder.bind(items[position] as DashboardAdapterItem.MovieList)
        }
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = when(items[position]) {
        is DashboardAdapterItem.Banner -> R.layout.item_banner
        is DashboardAdapterItem.Header -> R.layout.item_header
        is DashboardAdapterItem.MovieList -> R.layout.item_movie_list
        else -> error("unknown")
    }

    inner class HeaderViewHolder(
        private val binding: ItemHeaderBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(header: DashboardAdapterItem.Header) {
            binding.headerTitle.text = header.title
            binding.headerAction.setOnClickListener { onHeaderClick(header.title) }
        }
    }

    inner class BannerViewHolder(
        private val binding: ItemBannerBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(banner: DashboardAdapterItem.Banner) {
            binding.bannerImage.setImageResource(banner.imageURL)
        }
    }

    inner class MovieListViewHolder(
        private val binding: ItemMovieListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(movieList: DashboardAdapterItem.MovieList) {
            val adapter = DashboardMovieAdapter(imageLoader) { onListItemClick(it) }
            adapter.submitList(movieList.data)
            binding.movieRecyclerView.adapter = adapter
        }
    }
}