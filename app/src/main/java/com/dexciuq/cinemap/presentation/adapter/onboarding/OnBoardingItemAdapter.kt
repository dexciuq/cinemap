package com.dexciuq.cinemap.presentation.adapter.onboarding

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.dexciuq.cinemap.presentation.model.OnBoardingItem
import com.dexciuq.cinemap.presentation.screen.onboarding.OnBoardingItemFragment

class OnBoardingItemAdapter(
    fragmentActivity: FragmentActivity,
    private val onBoardingData: List<OnBoardingItem>
) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount() = onBoardingData.size

    override fun createFragment(position: Int): Fragment {
        val screen = onBoardingData[position]
        return OnBoardingItemFragment.newInstance(
            screen.animation,
            screen.title,
            screen.description,
        )
    }
}