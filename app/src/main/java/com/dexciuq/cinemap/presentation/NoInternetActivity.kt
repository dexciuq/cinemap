package com.dexciuq.cinemap.presentation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dexciuq.cinemap.appComponent
import com.dexciuq.cinemap.databinding.ActivityNoInternetBinding
import com.dexciuq.cinemap.di.DaggerAppComponent
import com.dexciuq.cinemap.util.NetworkUtils
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class NoInternetActivity : AppCompatActivity() {

    private val binding by lazy { ActivityNoInternetBinding.inflate(layoutInflater) }
    @Inject lateinit var networkUtils: NetworkUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        appComponent.inject(this)

        binding.reloadButton.setOnClickListener {
            if (networkUtils.isNetworkAvailable()) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Snackbar.make(binding.root, binding.noInternetTitle.text.toString(), Snackbar.LENGTH_SHORT).show()
            }
        }
    }
}