package com.dexciuq.cinemap.presentation.screen.favorite

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.dexciuq.cinemap.appComponent
import com.dexciuq.cinemap.databinding.FragmentFavoriteBinding
import com.dexciuq.cinemap.di.DaggerAppComponent
import com.dexciuq.cinemap.presentation.adapter.favorite.FavoritesAdapter
import com.dexciuq.cinemap.presentation.loader.ImageLoader
import javax.inject.Inject
import javax.inject.Named

class FavoriteFragment : Fragment() {

    @Inject
    @Named("favorite")
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var imageLoader: ImageLoader
    private val binding by lazy { FragmentFavoriteBinding.inflate(layoutInflater) }
    private val viewModel: FavoriteViewModel by viewModels { viewModelFactory }
    private val navController by lazy { findNavController() }
    private lateinit var adapter: FavoritesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        adapter = FavoritesAdapter(imageLoader) {
            val action = FavoriteFragmentDirections.actionFavoriteFragmentToDetailFragment(it)
            navController.navigate(action)
        }
        binding.movieRecyclerView.adapter = adapter

        val emptyScreen = listOf(
            binding.emptyListImage,
            binding.emptyListTitle,
            binding.emptyListDescription
        )
        viewModel.favorites.observe(viewLifecycleOwner) { movies ->
            adapter.submitList(movies)
            binding.progressBar.isVisible = false
            emptyScreen.forEach { it.isVisible = movies.isEmpty() }
        }
        return binding.root
    }
}