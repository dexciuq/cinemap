package com.dexciuq.cinemap.presentation.adapter.detail

import androidx.recyclerview.widget.DiffUtil
import com.dexciuq.cinemap.presentation.model.DetailAdapterItem

class DetailDiffCallback (
    private val oldList: List<DetailAdapterItem>,
    private val newList: List<DetailAdapterItem>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return when {
            oldItem is DetailAdapterItem.Backdrop && newItem is DetailAdapterItem.Backdrop ->
                oldItem.imageURL == newItem.imageURL
            oldItem is DetailAdapterItem.Option && newItem is DetailAdapterItem.Option ->
                oldItem.key == newItem.key
            oldItem is DetailAdapterItem.Cast && newItem is DetailAdapterItem.Cast ->
                oldItem.actors == newItem.actors
            else -> false
        }
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}