package com.dexciuq.cinemap.presentation.adapter.favorite

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.cinemap.data.model.local.MovieEntity
import com.dexciuq.cinemap.databinding.ItemMovieFullBinding
import com.dexciuq.cinemap.presentation.loader.ImageLoader

class FavoritesAdapter(
    private val imageLoader: ImageLoader,
    private val onItemClick: (Long) -> Unit,
) : RecyclerView.Adapter<FavoritesAdapter.ViewHolder>() {

    private var items: List<MovieEntity> = emptyList()

    fun submitList(newItems: List<MovieEntity>) {
        val diffResult = DiffUtil.calculateDiff(FavoritesCallback(items, newItems))
        items = newItems
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMovieFullBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(
        private val binding: ItemMovieFullBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(movieEntity: MovieEntity) {
            imageLoader.load(movieEntity.poster, binding.movieImage)
            binding.movieName.text = movieEntity.name
            binding.movieYear.text = movieEntity.year.toString()
            binding.movieGenres.text = movieEntity.genres
            binding.root.setOnClickListener { onItemClick(movieEntity.id) }
        }
    }
}