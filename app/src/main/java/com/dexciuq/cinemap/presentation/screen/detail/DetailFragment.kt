package com.dexciuq.cinemap.presentation.screen.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.dexciuq.cinemap.R
import com.dexciuq.cinemap.appComponent
import com.dexciuq.cinemap.databinding.FragmentDetailBinding
import com.dexciuq.cinemap.di.DaggerAppComponent
import com.dexciuq.cinemap.presentation.adapter.detail.DetailAdapter
import com.dexciuq.cinemap.presentation.loader.ImageLoader
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject
import javax.inject.Named

class DetailFragment : Fragment() {

    @Inject
    @Named("detail")
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var imageLoader: ImageLoader
    private val binding by lazy { FragmentDetailBinding.inflate(layoutInflater) }
    private val viewModel: DetailViewModel by viewModels { viewModelFactory }
    private val args: DetailFragmentArgs by navArgs()
    private lateinit var adapter: DetailAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().appComponent.inject(this)
        viewModel.setId(args.id)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.movie_about)
        adapter = DetailAdapter(
            imageLoader = imageLoader,
            deleteFromFavorites = { viewModel.deleteFromFavorites() },
            addToFavorites = {
                viewModel.addToFavorites()
                Snackbar.make(binding.root, "Успешно добавлено в вашему списку", Snackbar.LENGTH_SHORT).show()
            }
        )
        binding.detailRecyclerView.adapter = adapter

        viewModel.data.observe(viewLifecycleOwner, adapter::submitList)
        viewModel.isFavorite.observe(viewLifecycleOwner, adapter::setIsFavorite)

        viewModel.provideMovie()

        return binding.root
    }
}