package com.dexciuq.cinemap.presentation.adapter.detail

import androidx.recyclerview.widget.DiffUtil
import com.dexciuq.cinemap.data.model.remote.Person

class CastDiffCallback(
    private val oldList: List<Person>,
    private val newList: List<Person>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) : Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) : Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}