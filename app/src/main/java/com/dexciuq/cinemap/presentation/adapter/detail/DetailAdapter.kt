package com.dexciuq.cinemap.presentation.adapter.detail

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.cinemap.R
import com.dexciuq.cinemap.databinding.ItemBackdropBinding
import com.dexciuq.cinemap.databinding.ItemCastBinding
import com.dexciuq.cinemap.databinding.ItemOptionBinding
import com.dexciuq.cinemap.presentation.loader.ImageLoader
import com.dexciuq.cinemap.presentation.model.DetailAdapterItem

class DetailAdapter(
    private val imageLoader: ImageLoader,
    private val addToFavorites: () -> Unit,
    private val deleteFromFavorites: () -> Unit,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<DetailAdapterItem> = emptyList()
    private var isFavorite: Boolean = false

    fun setIsFavorite(boolean: Boolean) {
        isFavorite = boolean
    }

    fun submitList(newItems: List<DetailAdapterItem>) {
        val diffResult = DiffUtil.calculateDiff(DetailDiffCallback(items, newItems))
        items = newItems
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            R.layout.item_backdrop -> BackdropViewHolder(
                ItemBackdropBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
            )
            R.layout.item_option -> OptionViewHolder(
                ItemOptionBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
            )
            R.layout.item_cast -> CastViewHolder(
                ItemCastBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent, false
                )
            )
            else -> error("unknown viewType")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is BackdropViewHolder -> holder.bind(items[position] as DetailAdapterItem.Backdrop)
            is OptionViewHolder -> holder.bind(items[position] as  DetailAdapterItem.Option)
            is CastViewHolder -> holder.bind(items[position] as  DetailAdapterItem.Cast)
        }
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = when(items[position]) {
        is DetailAdapterItem.Backdrop -> R.layout.item_backdrop
        is DetailAdapterItem.Option -> R.layout.item_option
        is DetailAdapterItem.Cast -> R.layout.item_cast
    }

    inner class BackdropViewHolder(
        private val binding: ItemBackdropBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(backdrop: DetailAdapterItem.Backdrop) {
            imageLoader.load(backdrop.imageURL, binding.movieImage)
            binding.movieName.text = backdrop.name
            binding.movieInfo.text = backdrop.info
            binding.movieDescription.text = backdrop.description
            binding.favorite.setImageResource(
                if (isFavorite) R.drawable.ic_favorite
                else R.drawable.ic_favorite_border
            )

            binding.favorite.setOnClickListener {
                isFavorite = if (isFavorite) {
                    deleteFromFavorites()
                    binding.favorite.setImageResource(R.drawable.ic_favorite_border)
                    false
                } else {
                    addToFavorites()
                    binding.favorite.setImageResource(R.drawable.ic_favorite)
                    true
                }
            }
        }
    }

    inner class OptionViewHolder(
        private val binding: ItemOptionBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(option: DetailAdapterItem.Option) {
            binding.detailKey.text = "${option.key}:"
            binding.detailValue.text = option.value
        }
    }

    inner class CastViewHolder(
        private val binding: ItemCastBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(cast: DetailAdapterItem.Cast) {
            val adapter = CastAdapter(imageLoader)
            adapter.submitList(cast.actors)
            binding.castRecyclerView.adapter = adapter
        }
    }
}