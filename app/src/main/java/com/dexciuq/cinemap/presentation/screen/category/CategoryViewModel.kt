package com.dexciuq.cinemap.presentation.screen.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.cinemap.domain.usecase.GetGenresUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CategoryViewModel(
    private val getGenresUseCase: GetGenresUseCase
) : ViewModel() {

    private val _categories = MutableLiveData<List<String>>()
    val categories : LiveData<List<String>> = _categories

    fun provideCategories() = viewModelScope.launch(Dispatchers.IO) {
        _categories.postValue(getGenresUseCase.invoke())
    }
}