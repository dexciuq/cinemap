package com.dexciuq.cinemap.presentation.adapter.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.cinemap.databinding.ItemMovieShortBinding
import com.dexciuq.cinemap.domain.model.Movie
import com.dexciuq.cinemap.presentation.loader.ImageLoader

class DashboardMovieAdapter(
    private val imageLoader: ImageLoader,
    private val onItemClick: (Long) -> Unit
) : RecyclerView.Adapter<DashboardMovieAdapter.ViewHolder>() {

    private var items: List<Movie> = emptyList()

    fun submitList(newItems: List<Movie>) {
        val diffResult = DiffUtil.calculateDiff(MovieDiffCallback(items, newItems))
        items = newItems
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMovieShortBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(
        private val binding: ItemMovieShortBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: Movie) {
            imageLoader.load(movie.poster, binding.movieImage)
            binding.movieName.text = movie.name
            binding.root.setOnClickListener { onItemClick(movie.id) }
        }
    }
}