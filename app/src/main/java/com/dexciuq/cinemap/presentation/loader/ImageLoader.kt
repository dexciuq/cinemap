package com.dexciuq.cinemap.presentation.loader

import android.widget.ImageView

interface ImageLoader {
    fun load(url: String, imageView: ImageView)
}