package com.dexciuq.cinemap.presentation.screen.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.cinemap.domain.model.Movie
import com.dexciuq.cinemap.domain.usecase.AddFavoriteUseCase
import com.dexciuq.cinemap.domain.usecase.DeleteFavoriteUseCase
import com.dexciuq.cinemap.domain.usecase.GetFavoriteUseCase
import com.dexciuq.cinemap.domain.usecase.GetMovieUseCase
import com.dexciuq.cinemap.presentation.model.DetailAdapterItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel(
    private val getFavoriteUseCase: GetFavoriteUseCase,
    private val addFavoriteUseCase: AddFavoriteUseCase,
    private val deleteFavoriteUseCase: DeleteFavoriteUseCase,
    private val getMovieUseCase: GetMovieUseCase,
) : ViewModel() {

    private val _id = MutableLiveData<Long>()
    val id : LiveData<Long> = _id

    private val _movie = MutableLiveData<Movie>()

    private val _isFavorite = MutableLiveData<Boolean>()
    val isFavorite : LiveData<Boolean> = _isFavorite

    private val _data = MutableLiveData<List<DetailAdapterItem>>()
    val data : LiveData<List<DetailAdapterItem>> = _data

    fun setId(id: Long) {
        _id.value = id
    }

    fun provideMovie() = viewModelScope.launch(Dispatchers.IO) {
        val movie = getMovieUseCase(id.value!!)
        _movie.postValue(movie)

        val backdrop = DetailAdapterItem.Backdrop(
            movie.id, movie.backdrop, movie.name,
            "${movie.year}, ${movie.genres.first()}", movie.shortDescription,
        )

        val options = listOf(
            DetailAdapterItem.Option("Жанр", movie.genres.joinToString { it.capitalize() }),
            DetailAdapterItem.Option("Длительность", "${movie.duration} минут"),
            DetailAdapterItem.Option("Слоган", movie.slogan),
            DetailAdapterItem.Option("Бюджет", movie.budget),
            DetailAdapterItem.Option("Наборы по всему миру", movie.fees),
            DetailAdapterItem.Option("Рейтинг по КиноПоиск", movie.rating.kp.toString()),
            DetailAdapterItem.Option("Рейтинг по IMDB", movie.rating.imdb.toString()),
            DetailAdapterItem.Cast(movie.cast)
        )

        val result = mutableListOf<DetailAdapterItem>()
        result.add(backdrop)
        result.addAll(options)

        _isFavorite.postValue(isMovieExists(movie.id))
        _data.postValue(result)
    }

    private suspend fun isMovieExists(id: Long) = getFavoriteUseCase(id) != null

    fun addToFavorites() = viewModelScope.launch(Dispatchers.IO) {
        _movie.value?.let {
            addFavoriteUseCase(it)
            _isFavorite.postValue(true)
        }
    }

    fun deleteFromFavorites() = viewModelScope.launch(Dispatchers.IO) {
        _id.value?.let {
            deleteFavoriteUseCase(it)
            _isFavorite.postValue(false)
        }
    }
}