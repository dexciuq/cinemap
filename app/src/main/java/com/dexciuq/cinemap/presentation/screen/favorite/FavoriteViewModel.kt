package com.dexciuq.cinemap.presentation.screen.favorite

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dexciuq.cinemap.data.model.local.MovieEntity
import com.dexciuq.cinemap.domain.usecase.GetFavoritesUseCase

class FavoriteViewModel(
    private val getFavoritesUseCase: GetFavoritesUseCase
) : ViewModel() {

    val favorites : LiveData<List<MovieEntity>>
        get() = getFavoritesUseCase()
}