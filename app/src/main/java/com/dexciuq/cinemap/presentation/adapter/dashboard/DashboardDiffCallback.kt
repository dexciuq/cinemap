package com.dexciuq.cinemap.presentation.adapter.dashboard

import androidx.recyclerview.widget.DiffUtil
import com.dexciuq.cinemap.presentation.model.DashboardAdapterItem

class DashboardDiffCallback (
    private val oldList: List<DashboardAdapterItem>,
    private val newList: List<DashboardAdapterItem>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) : Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = oldList[oldItemPosition]

        return when {
            oldItem is DashboardAdapterItem.Banner && newItem is DashboardAdapterItem.Banner ->
                oldItem.imageURL == newItem.imageURL
            oldItem is DashboardAdapterItem.Header && newItem is DashboardAdapterItem.Header ->
                oldItem.title == newItem.title
            oldItem is DashboardAdapterItem.MovieList && newItem is DashboardAdapterItem.MovieList ->
                oldItem.data == newItem.data
            else -> false
        }
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) : Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]

        return when {
            oldItem is DashboardAdapterItem.Banner && newItem is DashboardAdapterItem.Banner ->
                oldItem == newItem
            oldItem is DashboardAdapterItem.Header && newItem is DashboardAdapterItem.Header ->
                oldItem == newItem
            oldItem is DashboardAdapterItem.MovieList && newItem is DashboardAdapterItem.MovieList ->
                oldItem == newItem
            else -> false
        }
    }
}