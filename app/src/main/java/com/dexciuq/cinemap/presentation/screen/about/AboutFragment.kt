package com.dexciuq.cinemap.presentation.screen.about

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dexciuq.cinemap.databinding.FragmentAboutBinding

class AboutFragment : Fragment() {

    private val binding by lazy { FragmentAboutBinding.inflate(layoutInflater) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        with(binding) {
            listOf(license, politics, info).forEach {
                it.setOnClickListener {
                    visitDocumentation()
                }
            }
        }
        return binding.root
    }

    private fun visitDocumentation() {
        val websiteUrl = "https://api.kinopoisk.dev/v1/documentation#/"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(websiteUrl))
        startActivity(intent)
    }
}