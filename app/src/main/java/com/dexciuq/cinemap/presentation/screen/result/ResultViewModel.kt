package com.dexciuq.cinemap.presentation.screen.result

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.cinemap.domain.model.Movie
import com.dexciuq.cinemap.domain.usecase.GetMoviesBySearchUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ResultViewModel(
    private val getMoviesBySearchUseCase: GetMoviesBySearchUseCase
) : ViewModel() {

    private val _query = MutableLiveData<String>()
    val query : LiveData<String> = _query

    private val _movies = MutableLiveData<List<Movie>>()
    val movies : LiveData<List<Movie>> = _movies

    fun setQuery(value: String) {
        _query.value = value
    }

    fun provideResult() = viewModelScope.launch(Dispatchers.IO) {
        val data = getMoviesBySearchUseCase(query.value!!)
        _movies.postValue(data)
    }
}