package com.dexciuq.cinemap.presentation.screen.onboarding

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.dexciuq.cinemap.databinding.FragmentOnBoardingItemBinding

private const val ARG_PARAM_ANIMATION = "animation"
private const val ARG_PARAM_TITLE = "title"
private const val ARG_PARAM_DESCRIPTION = "description"

class OnBoardingItemFragment : Fragment() {

    private val binding by lazy { FragmentOnBoardingItemBinding.inflate(layoutInflater) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        arguments?.let {
            binding.onBoardingItemAnimation.setAnimation(it.getInt(ARG_PARAM_ANIMATION))
            binding.onBoardingItemTitle.text = it.getString(ARG_PARAM_TITLE)
            binding.onBoardingItemDescription.text = it.getString(ARG_PARAM_DESCRIPTION)
        }
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance(animation: Int, title: String, description: String) =
            OnBoardingItemFragment().apply {
                arguments = bundleOf(
                    ARG_PARAM_ANIMATION to animation,
                    ARG_PARAM_TITLE to title,
                    ARG_PARAM_DESCRIPTION to description
                )
            }
    }
}