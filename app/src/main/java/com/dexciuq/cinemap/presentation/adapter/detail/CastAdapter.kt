package com.dexciuq.cinemap.presentation.adapter.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.cinemap.data.model.remote.Person
import com.dexciuq.cinemap.databinding.ItemPersonBinding
import com.dexciuq.cinemap.presentation.loader.ImageLoader

class CastAdapter(
    private val imageLoader: ImageLoader,
) : RecyclerView.Adapter<CastAdapter.ViewHolder>() {

    private var items: List<Person> = emptyList()

    fun submitList(newItems: List<Person>) {
        val diffResult = DiffUtil.calculateDiff(CastDiffCallback(items, newItems))
        items = newItems
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemPersonBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(
        private val binding: ItemPersonBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(person: Person) {
            imageLoader.load(person.photo, binding.personImage)
            binding.personName.text = person.name
            binding.personDescription.text = person.description
        }
    }
}