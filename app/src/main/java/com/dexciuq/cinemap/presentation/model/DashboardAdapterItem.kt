package com.dexciuq.cinemap.presentation.model

import com.dexciuq.cinemap.domain.model.Movie

sealed interface DashboardAdapterItem {
    data class Banner(
        val imageURL: Int,
    ) : DashboardAdapterItem

    data class Header(
        val title: String,
    ) : DashboardAdapterItem

    data class MovieList(
        val data: List<Movie>
    ) : DashboardAdapterItem
}