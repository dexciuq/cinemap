package com.dexciuq.cinemap.presentation

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.dexciuq.cinemap.util.Const
import com.dexciuq.cinemap.R
import com.dexciuq.cinemap.appComponent
import com.dexciuq.cinemap.databinding.ActivityOnBoardingBinding
import com.dexciuq.cinemap.di.DaggerAppComponent
import com.dexciuq.cinemap.presentation.adapter.onboarding.OnBoardingItemAdapter
import com.dexciuq.cinemap.presentation.model.OnBoardingItem
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject

class OnBoardingActivity : AppCompatActivity() {

    private val binding by lazy { ActivityOnBoardingBinding.inflate(layoutInflater) }
    @Inject lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        appComponent.inject(this)

        val onBoardingAdapter = OnBoardingItemAdapter(this, onBoardingItems)

        with(binding.onBoardingViewPager) {
            adapter = onBoardingAdapter
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    binding.nextButton.text =
                        if (position == onBoardingItems.size - 1) "Начать"
                        else "Далее"
                }
            })
        }

        TabLayoutMediator(binding.tabLayout, binding.onBoardingViewPager) { _, _ -> }.attach()

        binding.skipButton.setOnClickListener { completeOnBoarding() }

        binding.nextButton.setOnClickListener {
            val nextItem = binding.onBoardingViewPager.currentItem + 1
            if (nextItem < onBoardingAdapter.itemCount) binding.onBoardingViewPager.currentItem = nextItem
            else completeOnBoarding()
        }
    }

    private fun completeOnBoarding() {
        with(prefs.edit()) {
            putBoolean(Const.IS_ON_BOARDING_COMPLETED, true)
            apply()
        }
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    companion object {
        private val onBoardingItems = listOf(
            OnBoardingItem(R.raw.media_player, "Добро пожаловать в Cinemap",
                "Лучшее приложение для передачи фильмов века, которое сделает ваши дни замечательными."),
            OnBoardingItem(R.raw.man_watching_a_movie, "Открывайте для себя новое",
                "Изучите обширную коллекцию фильмов различных жанров. Найдите свой следующий любимый фильм с легкостью."),
            OnBoardingItem(R.raw.women_televison_views, "Наслаждайтесь просмотром",
                "Наслаждайтесь просмотром и погрузитесь в захватывающий мир кино и телесериалов с нашим приложением!"),
        )
    }
}