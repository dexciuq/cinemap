package com.dexciuq.cinemap.presentation.screen.result

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.dexciuq.cinemap.R
import com.dexciuq.cinemap.appComponent
import com.dexciuq.cinemap.databinding.FragmentResultBinding
import com.dexciuq.cinemap.di.DaggerAppComponent
import com.dexciuq.cinemap.presentation.adapter.dashboard.MovieAdapter
import com.dexciuq.cinemap.presentation.loader.ImageLoader
import com.dexciuq.cinemap.presentation.screen.seeall.SeeAllFragmentArgs
import com.dexciuq.cinemap.presentation.screen.seeall.SeeAllFragmentDirections
import com.dexciuq.cinemap.presentation.screen.seeall.SeeAllViewModel
import java.util.Locale
import javax.inject.Inject
import javax.inject.Named

class ResultFragment : Fragment() {

    @Inject
    @Named("result")
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var imageLoader: ImageLoader
    private val binding by lazy { FragmentResultBinding.inflate(layoutInflater) }
    private val viewModel: ResultViewModel by viewModels { viewModelFactory }
    private val navController by lazy { findNavController() }
    private val args: ResultFragmentArgs by navArgs()
    private lateinit var adapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().appComponent.inject(this)
        viewModel.setQuery(args.query)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        (activity as AppCompatActivity).supportActionBar?.title =
            viewModel.query.value?.replaceFirstChar {
                if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString()
            }

        adapter = MovieAdapter(imageLoader) {
            val action = ResultFragmentDirections.actionResultFragmentToDetailFragment(it)
            navController.navigate(action)
        }
        binding.resultRecyclerView.adapter = adapter

        viewModel.movies.observe(viewLifecycleOwner) {
            adapter.submitList(it)
            if (it.isEmpty()) {
                binding.searchError.isVisible = true
                binding.resultRecyclerView.isVisible = false
            }
            binding.progressBar.isVisible = false
        }
        viewModel.provideResult()
        return binding.root
    }
}