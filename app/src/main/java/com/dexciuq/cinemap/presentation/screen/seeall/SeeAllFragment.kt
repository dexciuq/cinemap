package com.dexciuq.cinemap.presentation.screen.seeall

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.dexciuq.cinemap.appComponent
import com.dexciuq.cinemap.databinding.FragmentSeeAllBinding
import com.dexciuq.cinemap.di.DaggerAppComponent
import com.dexciuq.cinemap.presentation.adapter.dashboard.MovieAdapter
import com.dexciuq.cinemap.presentation.loader.ImageLoader
import java.util.Locale
import javax.inject.Inject
import javax.inject.Named

class SeeAllFragment : Fragment() {

    @Inject
    @Named("seeall")
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var imageLoader: ImageLoader
    private val binding by lazy { FragmentSeeAllBinding.inflate(layoutInflater) }
    private val viewModel: SeeAllViewModel by viewModels { viewModelFactory }
    private val navController by lazy { findNavController() }
    private val args: SeeAllFragmentArgs by navArgs()
    private lateinit var adapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().appComponent.inject(this)
        viewModel.setGenre(args.genre)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        (activity as AppCompatActivity).supportActionBar?.title =
            viewModel.genre.value?.replaceFirstChar {
                if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString()
            }

        adapter = MovieAdapter(imageLoader) {
            val action = SeeAllFragmentDirections.actionSeeAllFragmentToDetailFragment(it)
            navController.navigate(action)
        }
        binding.genreRecyclerView.adapter = adapter

        viewModel.movies.observe(viewLifecycleOwner) {
            adapter.submitList(it)
            binding.progressBar.isVisible = false
        }
        viewModel.provideMovies()
        return binding.root
    }
}