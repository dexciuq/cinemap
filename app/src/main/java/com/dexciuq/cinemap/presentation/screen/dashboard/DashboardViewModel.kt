package com.dexciuq.cinemap.presentation.screen.dashboard

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.cinemap.R
import com.dexciuq.cinemap.domain.usecase.GetMoviesByGenreUseCase
import com.dexciuq.cinemap.presentation.model.DashboardAdapterItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.util.Locale


//private val MAIN_GENRES = listOf("фантастика", "комедия", "драма", "триллер", "криминал")
private val MAIN_GENRES = listOf("фантастика", "комедия")
private const val LIMIT = 20

class DashboardViewModel(
    private val getMoviesByGenreUseCase: GetMoviesByGenreUseCase
) : ViewModel() {

    private val _data = MutableStateFlow<List<DashboardAdapterItem>>(emptyList())
    val data = _data.asStateFlow()

    fun provideData() = viewModelScope.launch {
        val result = mutableListOf<DashboardAdapterItem>(
            DashboardAdapterItem.Banner(R.drawable.placeholder_banner)
        )
        MAIN_GENRES.forEach { genre ->
            val header = DashboardAdapterItem.Header(
                genre.replaceFirstChar {
                    if (it.isLowerCase()) it.titlecase(Locale.ROOT)
                    else it.toString()
                }
            )
            val movies = DashboardAdapterItem.MovieList(getMoviesByGenreUseCase(genre, LIMIT))
            result.add(header)
            result.add(movies)
        }
        _data.emit(result)
    }
}