package com.dexciuq.cinemap.presentation.model

data class OnBoardingItem(
    val animation: Int,
    val title: String,
    val description: String
)