package com.dexciuq.cinemap.presentation.adapter.favorite

import androidx.recyclerview.widget.DiffUtil
import com.dexciuq.cinemap.data.model.local.MovieEntity

class FavoritesCallback(
    private val oldList: List<MovieEntity>,
    private val newList: List<MovieEntity>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) : Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) : Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}