package com.dexciuq.cinemap.presentation

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.dexciuq.cinemap.util.Const
import com.dexciuq.cinemap.R
import com.dexciuq.cinemap.appComponent
import com.dexciuq.cinemap.databinding.ActivityMainBinding
import com.dexciuq.cinemap.di.DaggerAppComponent
import com.dexciuq.cinemap.util.NetworkUtils
import com.google.android.material.bottomnavigation.BottomNavigationView
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    @Inject lateinit var prefs: SharedPreferences
    @Inject lateinit var networkUtils: NetworkUtils
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        installSplashScreen()

        if (!prefs.getBoolean(Const.IS_ON_BOARDING_COMPLETED, false)) {
            val intent = Intent(this, OnBoardingActivity::class.java)
            startActivity(intent)
            finish()
            return
        }

        if (!networkUtils.isNetworkAvailable()) {
            val intent = Intent(this, NoInternetActivity::class.java)
            startActivity(intent)
            finish()
            return
        }

        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        setupBottomNavigationView()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }

    private fun setupBottomNavigationView() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(
                R.id.nav_host_fragment_container
            ) as NavHostFragment

        navController = navHostFragment.navController
        binding.bottomNavigationView.setupWithNavController(navController)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_dashboard,
                R.id.navigation_category,
                R.id.navigation_favorite,
                R.id.navigation_about,
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
    }
}