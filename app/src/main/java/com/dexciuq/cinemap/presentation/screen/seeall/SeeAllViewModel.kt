package com.dexciuq.cinemap.presentation.screen.seeall

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.cinemap.domain.model.Movie
import com.dexciuq.cinemap.domain.usecase.GetMoviesByGenreUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SeeAllViewModel(
    private val getMoviesByGenreUseCase: GetMoviesByGenreUseCase
) : ViewModel() {

    private val _genre = MutableLiveData<String>()
    val genre : LiveData<String> = _genre

    private val _movies = MutableLiveData<List<Movie>>()
    val movies : LiveData<List<Movie>> = _movies

    fun setGenre(value: String) {
        _genre.value = value
    }

    fun provideMovies() = viewModelScope.launch(Dispatchers.IO) {
        val data = getMoviesByGenreUseCase(genre.value!!, 250)
        _movies.postValue(data)
    }
}