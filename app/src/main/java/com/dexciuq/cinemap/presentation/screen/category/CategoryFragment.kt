package com.dexciuq.cinemap.presentation.screen.category

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.dexciuq.cinemap.appComponent
import com.dexciuq.cinemap.databinding.FragmentCategoryBinding
import com.dexciuq.cinemap.di.DaggerAppComponent
import com.dexciuq.cinemap.presentation.adapter.category.CategoryAdapter
import javax.inject.Inject
import javax.inject.Named

class CategoryFragment : Fragment() {

    @Inject
    @Named("category")
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val binding by lazy { FragmentCategoryBinding.inflate(layoutInflater) }
    private val viewModel: CategoryViewModel by viewModels { viewModelFactory }
    private lateinit var adapter: CategoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        adapter = CategoryAdapter {
            val action = CategoryFragmentDirections.actionCategoryFragmentToSeeAllFragment(it.lowercase())
            findNavController().navigate(action)
        }
        binding.categoryRecyclerView.adapter = adapter

        viewModel.categories.observe(viewLifecycleOwner) {
            adapter.submitList(it)
            binding.progressBar.isVisible = false
        }
        viewModel.provideCategories()

        return binding.root
    }
}