package com.dexciuq.cinemap.presentation.screen.dashboard

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.dexciuq.cinemap.appComponent
import com.dexciuq.cinemap.databinding.FragmentDashboardBinding
import com.dexciuq.cinemap.di.DaggerAppComponent
import com.dexciuq.cinemap.presentation.adapter.dashboard.DashboardAdapter
import com.dexciuq.cinemap.presentation.loader.ImageLoader
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

class DashboardFragment : Fragment() {

    @Inject
    @Named("dashboard")
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var imageLoader: ImageLoader
    private val binding by lazy { FragmentDashboardBinding.inflate(layoutInflater) }
    private val viewModel: DashboardViewModel by viewModels { viewModelFactory }
    private val navController by lazy { findNavController() }
    private lateinit var adapter: DashboardAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        adapter = DashboardAdapter(
            imageLoader = imageLoader,
            onHeaderClick = {
                val action = DashboardFragmentDirections.actionDashboardFragmentToSeeAllFragment(it.lowercase())
                navController.navigate(action)
            },
            onListItemClick = {
                val action = DashboardFragmentDirections.actionDashboardFragmentToDetailFragment(it)
                navController.navigate(action)
            }
        )
        binding.mainRecyclerView.adapter = adapter

        binding.searchText.setOnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_DOWN) {
                val inputMethodManager = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(binding.searchText.windowToken, 0)

                val query = binding.searchText.text.toString()
                val action = DashboardFragmentDirections.actionDashboardFragmentToResultFragment(query)
                navController.navigate(action)
                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        }

        lifecycleScope.launch {
            viewModel.data.collect {
                binding.progressBar.isVisible = false
                adapter.submitList(it)
            }
        }

        viewModel.provideData()

        return binding.root
    }
}