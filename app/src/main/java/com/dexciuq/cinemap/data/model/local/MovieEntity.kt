package com.dexciuq.cinemap.data.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movies")
data class MovieEntity(
    @PrimaryKey val id: Long,
    val name: String,
    val year: Int,
    val genres: String,
    val poster: String,
)
