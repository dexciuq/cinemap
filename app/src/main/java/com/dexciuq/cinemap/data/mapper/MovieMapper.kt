package com.dexciuq.cinemap.data.mapper

import com.dexciuq.cinemap.data.model.remote.MovieDto
import com.dexciuq.cinemap.domain.model.Movie
import com.dexciuq.cinemap.util.Const
import javax.inject.Inject

class MovieMapper @Inject constructor() : Mapper<MovieDto, Movie>   {

    override fun transform(param: MovieDto) = Movie(
        id = param.id.toLong(),
        name = param.name.orEmpty(),
        alternativeName = param.alternativeName.orEmpty(),
        description = param.description.orEmpty(),
        shortDescription = param.shortDescription.orEmpty(),
        slogan = param.slogan.orEmpty(),
        year = param.year,
        countries = param.countries.map { it.name },
        duration = param.duration,
        poster = param.poster?.url ?: Const.POSTER_PLACEHOLDER,
        backdrop = param.backdrop?.url.orEmpty(),
        logo = param.logo?.url.orEmpty(),
        genres = param.genres.map { it.name },
        cast = param.cast.orEmpty(),
        budget = "${param.budget?.currency} ${param.budget?.value}",
        fees = "${param.fees?.world?.currency} ${param.fees?.world?.value}",
        rating = param.rating,
        votes = param.votes,
    )
}