package com.dexciuq.cinemap.data.datasource.remote

import com.dexciuq.cinemap.data.datasource.MovieDataSource
import com.dexciuq.cinemap.data.mapper.GenreMapper
import com.dexciuq.cinemap.data.mapper.Mapper
import com.dexciuq.cinemap.data.mapper.MovieMapper
import com.dexciuq.cinemap.data.model.remote.GenresDtoItem
import com.dexciuq.cinemap.data.model.remote.MovieDto
import com.dexciuq.cinemap.domain.model.Movie
import javax.inject.Inject

class KinoPoiskDataSource @Inject constructor(
    private val movieMapper: MovieMapper,
    private val genreMapper: GenreMapper,
    private val apiService: KinoPoiskApiService,
) : MovieDataSource.Remote {

    override suspend fun getMovie(id: Long): Movie = movieMapper.transform(
        apiService.getMovie(id)
    )

    override suspend fun getMoviesBySearch(query: String): List<Movie> = movieMapper.transformAll(
        apiService.getMoviesBySearch(query).movies
    )

    override suspend fun getMoviesByGenre(genre: String, limit: Int): List<Movie> = movieMapper.transformAll(
        apiService.getMoviesByGenre(genre, limit).movies
    )

    override suspend fun getGenres(): List<String> = genreMapper.transformAll(
        apiService.getGenres()
    )
}