package com.dexciuq.cinemap.data.mapper

import com.dexciuq.cinemap.data.model.remote.GenresDtoItem

import javax.inject.Inject

class GenreMapper @Inject constructor() : Mapper<GenresDtoItem, String>  {

    override fun transform(param: GenresDtoItem): String = param.name
}