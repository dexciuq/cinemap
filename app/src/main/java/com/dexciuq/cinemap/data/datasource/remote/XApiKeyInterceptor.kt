package com.dexciuq.cinemap.data.datasource.remote

import okhttp3.Interceptor
import okhttp3.Response

class XApiKeyInterceptor(private val token: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .addHeader("X-API-KEY", token)
            .build()
        return chain.proceed(request)
    }
}