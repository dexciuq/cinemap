package com.dexciuq.cinemap.data.datasource

import androidx.lifecycle.LiveData
import com.dexciuq.cinemap.data.model.local.MovieEntity
import com.dexciuq.cinemap.domain.model.Movie

interface MovieDataSource {
    interface Remote {
        suspend fun getMovie(id: Long): Movie
        suspend fun getMoviesBySearch(query: String): List<Movie>
        suspend fun getMoviesByGenre(genre: String, limit: Int): List<Movie>
        suspend fun getGenres(): List<String>
    }
    interface Local {
        fun getFavorites(): LiveData<List<MovieEntity>>
        suspend fun getMovieById(id: Long): MovieEntity?
        suspend fun addToFavorites(movie: Movie)
        suspend fun deleteFromFavorites(id: Long)
    }
}