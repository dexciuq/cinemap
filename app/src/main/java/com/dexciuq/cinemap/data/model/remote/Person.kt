package com.dexciuq.cinemap.data.model.remote

import com.google.gson.annotations.SerializedName

data class Person(
    @SerializedName("id") val id: Long,
    @SerializedName("photo") val photo: String,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
)
