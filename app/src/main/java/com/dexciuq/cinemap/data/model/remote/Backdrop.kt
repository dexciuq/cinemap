package com.dexciuq.cinemap.data.model.remote

import com.google.gson.annotations.SerializedName

data class Backdrop(
    @SerializedName("url") val url: String,
)