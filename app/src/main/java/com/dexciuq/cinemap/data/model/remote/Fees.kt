package com.dexciuq.cinemap.data.model.remote

import com.google.gson.annotations.SerializedName

data class Fees(
    @SerializedName("world") val world: Budget,
    @SerializedName("russia") val russia: Budget,
    @SerializedName("usa") val usa: Budget
)