package com.dexciuq.cinemap.data.model.remote

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

//@Keep
data class MovieDto(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String?,
    @SerializedName("description") val description: String?,
    @SerializedName("year") val year: Int,
    @SerializedName("poster") val poster: Poster?,
    @SerializedName("backdrop") val backdrop: Backdrop?,
    @SerializedName("logo") val logo: Logo?,
    @SerializedName("slogan") val slogan: String?,
    @SerializedName("genres") val genres: List<Genre>,
    @SerializedName("movieLength") val duration: Int,
    @SerializedName("rating") val rating: Rating,
    @SerializedName("votes") val votes: Votes,
    @SerializedName("countries") val countries: List<Country>,
    @SerializedName("alternativeName") val alternativeName: String?,
    @SerializedName("shortDescription") val shortDescription: String?,
    @SerializedName("persons") val cast: List<Person>?,
    @SerializedName("budget") val budget: Budget?,
    @SerializedName("fees") val fees: Fees?,
)