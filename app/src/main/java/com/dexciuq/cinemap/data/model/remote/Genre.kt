package com.dexciuq.cinemap.data.model.remote

import com.google.gson.annotations.SerializedName

data class Genre(
    @SerializedName("name") val name: String
)