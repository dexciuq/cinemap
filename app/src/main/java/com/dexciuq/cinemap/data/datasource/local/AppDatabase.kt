package com.dexciuq.cinemap.data.datasource.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dexciuq.cinemap.data.model.local.MovieEntity

@Database(entities = [MovieEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}