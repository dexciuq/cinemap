package com.dexciuq.cinemap.data.model.remote

import com.google.gson.annotations.SerializedName

data class Logo(
    @SerializedName("url") val url: String?
)