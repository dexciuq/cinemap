package com.dexciuq.cinemap.data.datasource.remote

import com.dexciuq.cinemap.data.model.remote.GenresDto
import com.dexciuq.cinemap.data.model.remote.MovieDto
import com.dexciuq.cinemap.data.model.remote.SearchDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface KinoPoiskApiService {

    @GET("/v1.3/movie/{id}")
    suspend fun getMovie(@Path("id") id: Long): MovieDto

    @GET("/v1.3/movie")
    suspend fun getMoviesByGenre(
        @Query("genres.name") genre: String,
        @Query("limit") limit: Int = 20,
    ): SearchDto

    @GET("/v1.3/movie")
    suspend fun getMoviesBySearch(
        @Query("name") name: String,
        @Query("limit") limit: Int = 20,
    ): SearchDto

    @GET("/v1/movie/possible-values-by-field")
    suspend fun getGenres(@Query("field") field: String = "genres.name"): GenresDto
}