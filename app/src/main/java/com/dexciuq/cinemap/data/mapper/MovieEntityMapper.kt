package com.dexciuq.cinemap.data.mapper

import com.dexciuq.cinemap.data.model.local.MovieEntity
import com.dexciuq.cinemap.domain.model.Movie
import javax.inject.Inject

class MovieEntityMapper @Inject constructor() : Mapper<Movie, MovieEntity>  {
    override fun transform(param: Movie) = MovieEntity(
        id = param.id,
        name = param.name,
        year = param.year,
        genres = param.genres.joinToString { it.capitalize() },
        poster = param.poster
    )
}