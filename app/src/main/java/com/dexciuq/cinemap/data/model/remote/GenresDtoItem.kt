package com.dexciuq.cinemap.data.model.remote


import com.google.gson.annotations.SerializedName

data class GenresDtoItem(
    @SerializedName("name")
    val name: String,
    @SerializedName("slug")
    val slug: String
)