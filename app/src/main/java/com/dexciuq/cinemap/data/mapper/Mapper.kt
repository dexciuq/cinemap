package com.dexciuq.cinemap.data.mapper

interface Mapper<in P, out R> {
    fun transform(param: P): R
    fun transformAll(params: List<P>): List<R> = params.map(::transform)
}