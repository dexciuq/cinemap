package com.dexciuq.cinemap.data.datasource.local

import androidx.lifecycle.LiveData
import com.dexciuq.cinemap.data.model.local.MovieEntity
import com.dexciuq.cinemap.data.datasource.MovieDataSource
import com.dexciuq.cinemap.data.mapper.Mapper
import com.dexciuq.cinemap.data.mapper.MovieEntityMapper
import com.dexciuq.cinemap.domain.model.Movie
import javax.inject.Inject

class LocalDataSource @Inject constructor(
    private val dao: MovieDao,
    private val mapper: MovieEntityMapper
) : MovieDataSource.Local {

    override fun getFavorites(): LiveData<List<MovieEntity>> = dao.getAll()

    override suspend fun getMovieById(id: Long): MovieEntity? = dao.getMovieById(id)

    override suspend fun addToFavorites(movie: Movie) = dao.insert(mapper.transform(movie))

    override suspend fun deleteFromFavorites(id: Long) = dao.deleteById(id)
}