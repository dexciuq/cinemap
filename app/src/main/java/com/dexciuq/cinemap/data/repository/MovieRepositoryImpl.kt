package com.dexciuq.cinemap.data.repository

import com.dexciuq.cinemap.data.datasource.MovieDataSource
import com.dexciuq.cinemap.domain.model.Movie
import com.dexciuq.cinemap.domain.repository.MovieRepository
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val remote: MovieDataSource.Remote,
    private val local: MovieDataSource.Local,
) : MovieRepository {
    override suspend fun getMovie(id: Long) = remote.getMovie(id)
    override suspend fun getMoviesBySearch(query: String) = remote.getMoviesBySearch(query)
    override suspend fun getMoviesByGenre(genre: String, limit: Int) = remote.getMoviesByGenre(genre, limit)
    override suspend fun getGenres(): List<String> = remote.getGenres()
    override fun getFavorites() = local.getFavorites()
    override suspend fun getFavorite(id: Long) = local.getMovieById(id)
    override suspend fun addToFavorites(movie: Movie) = local.addToFavorites(movie)
    override suspend fun deleteFromFavorites(id: Long) = local.deleteFromFavorites(id)
}