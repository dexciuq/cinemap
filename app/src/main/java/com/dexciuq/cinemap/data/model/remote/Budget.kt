package com.dexciuq.cinemap.data.model.remote

import com.google.gson.annotations.SerializedName

data class Budget(
    @SerializedName("value") val value: Long,
    @SerializedName("currency") val currency: String
)
