package com.dexciuq.cinemap.util

object Const {
    const val SHARED_PREF_NAME = "cinemap_prefs"
    const val IS_ON_BOARDING_COMPLETED = "isOnBoardingCompleted"

    const val POSTER_PLACEHOLDER = "https://yastatic.net/s3/kinopoisk-frontend/common-static/img/projector-logo/placeholder.svg"
    const val DATABASE_NAME = "cinemap.db"

    const val BASE_URL = "https://api.kinopoisk.dev"
    const val API_KEY = "Z9XFRXP-ZZY4W8P-HR2TWXK-RF2X4AQ"
}