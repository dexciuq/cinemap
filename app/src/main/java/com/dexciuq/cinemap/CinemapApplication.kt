package com.dexciuq.cinemap

import android.app.Application
import android.content.Context
import com.dexciuq.cinemap.di.AppComponent
import com.dexciuq.cinemap.di.DaggerAppComponent

class CinemapApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .bindContext(this)
            .build()
    }
}

val Context.appComponent: AppComponent
    get() = when(this) {
        is CinemapApplication -> appComponent
        else -> applicationContext.appComponent
    }